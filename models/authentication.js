const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");

const authenticationSchema = mongoose.Schema({
	userId: {type: String, required: true},
	SSOToken: {type: String, required: true, unique: true},
	origin: {type: String, required: true},
	connectedAt: {type: Number},
	expiresAt: {type: Number},
	isExpired: {type: Boolean},
}, {
	writeConcern: {
		w: "majority",
		j: true,
		wtimeout: 1000
	}
});

authenticationSchema.plugin(uniqueValidator);

module.exports = mongoose.model("Authentication", authenticationSchema);