const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");

/**
 * Modèle de donnée simple, juste pour montrer ce qu'on peut faire avec
 * passport.
 * On ajoute un attribut state, pour le traitement interne. Il peut avoir plusieurs valeurs :
 *  valid
 *  canMerge --> si l'utilisateur à plusieur moyen de se connecter à la même adresse mail
 *  mailConflict --> si on a plusieurs adresses mail sur le même user
 *  toCreate --> on doit créer le compte.
 *
 *  Pour la partie writeConcern, je vous pose là la doc mangoDB, que je n'ai moi même pas bien compris :
 *  the w option to request acknowledgment that the write operation has propagated to a specified number of mongod instances or to mongod instances with specified tags.
 *  the j option to request acknowledgment that the write operation has been written to the on-disk journal, and
 *  the wtimeout option to specify a time limit to prevent write operations from blocking indefinitely.
 *
 *  Suite aux tests réalisés avec l'ami Guillaume FOULON, il a été décidé d'enlever la nécessité d'avoir un password
 *  obligatoire, au moins temporairement.
 */
const userSchema = mongoose.Schema({
	email: {type: String, required: true, unique: true},
	password: {type: String},
	facebook_id: {type: String, unique: true},
	google_id: {type: String, unique: true},
	linkedin_id: {type: String, unique: true},
	twitter_id: {type: String, unique: true},
	state: {type: String},
}, {
	writeConcern: {
		w: "majority",
		j: true,
		wtimeout: 1000
	}
});

userSchema.plugin(uniqueValidator);

module.exports = mongoose.model("User", userSchema);