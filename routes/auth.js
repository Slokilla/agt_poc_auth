const express = require("express");
const router = express.Router();
const passport = require("passport");
const jwt = require("jsonwebtoken");
const Authentication = require("../models/authentication");


//region Outils
/**
 * Dans toutes les routes, il est important de tester si l'utilisateur n'est pas déjà login
 * @param req
 * @param res
 * @param next
 */
const checkAuth = (req, res, next) => {
	if (!req.user) {
		next();
	} else {
		res.redirect("/");
	}
};

/**
 * Dans toute les routes, elle sert à vérifier que la connexion de l'utilisateur n'a pas expiré.
 *
 * CF controller/auth.js pour savoir comment on détermine si un token a expriré ou non
 *
 * @param req
 * @param res
 * @param next
 */
const checkExpire = (req, res, next) => {
	if (!req.user && !req.user.error) {
		next();
	} else {
		if (req.user.error === "expiredToken") {
			req.session.destroy();
		}
		res.redirect("/");
	}
};


const {merge, finalize} = require("../controller/accountStrat/mergeAccount");
const {create} = require("../controller/accountStrat/createAccount");
const {login} = require("../controller/SSO_Engine/login");
//endregion

//region Deconnexion
router.get("/logout", (req, res) => {
	if (req.user) {
		console.log(req.user);
		Authentication.findOne({SSOToken: req.user.ssoToken})
			.then((usr) => {
				usr.isExpired = true;
				usr.save(err => {
					if (err) {
						console.log("Erreur lors de la déconnexion !");
						res.status(500).render("error", {
							message: "Une erreur est survenue lors de la déconnexion",
							error: {
								status: 500,
								stack: err
							}
						});
					}
				});
			}).catch(err => {
				console.log("Aucune authentification n'a été trouvée ! ");
				res.status(404).render("error", {
					message: "Aucune authentification n'a été trouvée ! ",
					error: {
						status: 404,
						stack: err
					}
				});
			});

		req.logOut();
	}
	res.redirect("/");
});
//endregion

//region Authentification local
/**
 * Route dédiée à la connexion. Elle commence par checker que l'user n'est pas déjà connecté.
 * GET {
 *     Avec cette méthode, on envoie l'utilsateur arrivé par un lien vers la page d'inscription/connexion
 * }
 * POST {
 *     On essaie ici d'authentifier l'utilisateur, avec une redirection en cas de mauvaise connexion, vers la page login.
 *     Normalement, il arrive sur cette page avec un message chargé en flash
 *     On arrive dans la dernière fonction seulement si l'utilisateur est authentifié
 * }
 */
router.route("/login")
	.get(login)
	.post(passport.authenticate("local", {failureRedirect: "/auth/login",}), login);

/*
***WIP***
router.route('/signin')
    .get((req, res, next) => {
        res.render('auth', {title: 'Connexion'});
    })
    .post(passport.authenticate('local'),(req, res) => {res.send(req.user)});
*/
//endregion

//region Authentification google
/**
 * L'authentification avec google se fait en deux temps. D'abord, l'utilisateur arrive sur l'url /google, passport est
 * configuré, et l'utilisateur est instantanément redirigé vers une page ou il choisi un compte avec lequel s'authentifier.
 * Une fois le compte choisi :
 * SI l'authentification s'est bien passée,
 *      l'utilisateur arrive sur notre callback, qui renvoie vers l'accueil après un potentiel traitement.
 * SINON,
 *      l'user est redirigé vers la page de login.
 * FinSI
 */
router.get("/google", checkAuth, passport.authenticate("google", {
	failureRedirect: "/auth/login"
}));

router.get("/google/callback", checkAuth, passport.authenticate("google"), create, merge, login);
//endregion

//region Authentification facebook
/**
 * Fonctionnement similaire à google.
 */
router.get("/facebook", checkAuth, passport.authenticate("facebook", undefined, undefined));

router.get("/facebook/callback", checkAuth, passport.authenticate("facebook", {failureRedirect: "/auth/login"}), create, merge, login);
//endregion

//region Authentification linkedin
/**
 * Fonctionnement similaire à google, on utilise la version de la statégie basée sur Oauth 2.
 */
router.get("/linkedin", checkAuth, passport.authenticate("linkedin", {failureRedirect: "/auth/login"}, undefined));

router.get("/linkedin/callback", checkAuth, passport.authenticate("linkedin"), create, merge, login);
//endregion

//region Authentification twitter
/**
 * Fonctionnement similaire à google.
 */
router.get("/twitter", checkAuth, passport.authenticate("twitter", {failureRedirect: "/auth/login"}, err => checkExpire(err)));

router.get("/twitter/callback", checkAuth, passport.authenticate("twitter"), create, merge, login);
//endregion


router.get("/merge", finalize);

module.exports = router;