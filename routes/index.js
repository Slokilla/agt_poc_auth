const express = require("express");
const router = express.Router();
const axios = require("axios");

/* GET home page. */
router.get("/", async (req, res) => {
	if (req.user) {
		await axios.get(`${process.env.DOMAIN_URL}cas/profile?t=${req.user.ssoToken}`)
			.then(response => res.render("index", {title: "Connecté", user: response.data}))
			.catch(err => {
				console.log("somethind happened");
				console.log(err);
				Promise.reject(err);
			});

	} else {
		res.render("index", {title: "Express"});
	}
});

module.exports = router;
