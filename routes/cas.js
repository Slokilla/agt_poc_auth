const express = require("express");
const router = express.Router();
const {isAuth, profile} = require("../controller/SSO_Engine/transactions");

/* GET home page. */
router.get("/isAuth", isAuth);
router.get("/profile", profile);

module.exports = router;
