//region IMPORTS
//region Outils
const createError = require("http-errors");
const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const session = require("express-session");
const logger = require("morgan");
const mongoose = require("mongoose");
const {clearConnections} = require("./controller/server/startup");
require("dotenv").config();
//endregion

//region Gestion des authentifications.
const passport = require("passport");
// eslint-disable-next-line no-unused-vars
const authConfig = require("./controller/accountStrat/auth");
//endregion

//region Routes
const indexRouter = require("./routes/index");
const usersRouter = require("./routes/users");
const authRouter = require("./routes/auth");
const casRouter = require("./routes/cas");
//endregion

const app = express();
//endregion

//region DB Connection
mongoose.connect(process.env.MONGO_CONNECTION_STRING,
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	})
	.then(() => {
		console.log("Connexion à MongoDB réussie !");
		// Solution pour déconnecter tout les utilisateurs en cas de crash serveur
		if (process.env.RESET_SESSIONS_ON_RESTART === true) {
			console.log("Réinitialisation des connexions actives !");
			clearConnections().then(() =>
				console.log("Connexions réinitialisées !")
			);
		}
		console.log("MongoDB prêt à l'usage !");
	})
	.catch(() => console.log("Connexion à MongoDB échouée !"));
//endregion 

//region View engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "twig");
//endregion

//region Outils
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(session({
	secret: [process.env.SESSION_KEY],
	saveUninitialized: true,
	resave: false,
	cookie: {
		secure: false, //Corromps les cookie si set à true et GET une connexion non HTTPS
		maxAge: 60 * 60 * 1000, //Shortcut pour Expires : Date.now() + X
	}
}));

app.use(cookieParser());

app.use(express.static(path.join(__dirname, "public")));
//endregion

//region Passport
app.use(passport.initialize(undefined));
app.use(passport.session(undefined));
//endregion

//region Routes
app.use("/", indexRouter);
app.use("/auth", authRouter);
app.use("/users", usersRouter);
app.use("/cas", casRouter);
//endregion

//region catch 404 and forward to error handler
app.use(function (req, res, next) {
	next(createError(404));
});
//endregion


//region error handler
app.use(function (err, req, res, next) {
	// set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = req.app.get("env") === "development" ? err : {};

	// render the error page
	res.status(err.status || 500);
	res.render("error");
});
//endregion

module.exports = app;
