**Proof of concept AUTHENTIFICATION**
========
V1
--------
**Moteur de SSO**
Le coeur de la chose fait dans controller/login.js. 
    
    
   * Moteur d'authentification
   - [X] Récupération de l'origine de la requête
   - [X] Connexion de l'utilisateur
   - [X] Génération d'un identifiant pour une connexion
   - [X] Avec plusieurs domaines d'origine, un user est reconnu, et donc on lui renvoie
   le même token
   - [X] Whitelist des origines
   - [X] Compatible indépendamment de la stratégie de connexion
   - [X] Déconnecte tous les utilisateurs en cas de crash server. 
   
   * Transactions d'information
   - [X] Echanger le token contre des données utiles
   - [X] Garder une trace des connexion et des origines
   - [X] Rafraichir les token expirés
   - [X] Facilité de création de nouvelles transactions
   
Le sso a été réaliser en partie [grâce à ce tuto](https://codeburst.io/building-a-simple-single-sign-on-sso-server-and-solution-from-scratch-in-node-js-ea6ee5fdf340#f198).
Un autre source d'inspiration a été l'étude des cookies des différents sites google. Ils n'ont en effet qu'un seul cookie en commun (pour youtube, account.google.com, et google.com). De là je me suis dit qu'il s'agissait sûrement d'un token associé en base à un user coté serveur sso , et qui était échangé lors d'appel des différents sites. Ses sites enregistrent ensuite en cache les infos en question. 

**Moteur d'authentification**
* En général :
    - [X] Liaison à une base de données.
    - [x] Connexion d'un utilisateur et récupération de ses données.
    - [x] comportement différent en fonction de la présence 
    d'un utilisateur connecté ou non.
    - [x] intégration de JWT

* Google : 
    - [x] Connexion et configuration
    - [x] Récupération des infos utilisateurs (*Nom, Prénom, Photo*)
    - [X] Récupération d'une adresse mail
    - [X] Forcer l'utilisateur à choisir un compte
    - [X] Lier fortement un compte google et un compte SIMEOS pour se désolidariser 
    au maximum de google. 
    
* Facebook : 
    - [x] Connexion et configuration
    - [X] Récupération des infos utilisateurs (*Nom, Prénom, Photo*)
    - [X] Récupération d'une adresse mail    
    - [X] Forcer l'utilisateur à choisir un compte

    
* LinkedIn : 
    - [x] Connexion et configuration
    - [X] Récupération des infos utilisateurs (*Nom, Prénom, Photo*)
    - [X] Récupération d'une adresse mail
    - [X] Merge quand le compte existe déjà. 
         
* Twitter : 
    - [x] Connexion et configuration
    - [X] Récupération des infos utilisateurs (*Nom, Prénom, Photo*)
    - [X] Récupération d'une adresse mail
    - [X] Merge quand le compte existe déjà. 
 
** A propos de JWT **
 
Mes recherches m'ont mené à étudier JWT plus en détails, et j'ai pu découvrir plusieurs limites
de cette techno, avant de voir qu'une bonne samaritaine en a fait un billet de blog. En voici donc [le lien](https://medium.com/swlh/hacking-json-web-tokens-jwts-9122efe91e4a). 
    
**Remarques**

Passport est très utile, même en local. Il nous permet de remplacer 
un éventuel middleware d'authentification par un *(ou plusieurs)* modules testés.
Si la doc est un peu maigre, on appréciera l'existence d'une communauté, qui est 
plutôt active. 

J'ai trouvé, sur le tard, une série de vidéos sur l'intégration d'une 
stratégie google dans un projet. Ce n'est pas mon genre *(beaucoup trop long)*
mais n'hésitez pas à y [jeter un oeil](https://www.youtube.com/watch?v=sakQbeRjgwg&list=PL4cUxeGkcC9jdm7QX143aMLAqyM-jTZ2x "Playlist tuto passport") !