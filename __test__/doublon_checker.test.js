/* eslint-disable no-undef */
const faker = require("faker"); //Générateur de données cohérentes.
const User = require("../models/user");
const doublonCheck = require("../controller/accountStrat/doublon_checker").doublonCheck;
const states = require("../controller/accountStrat/AccountStates");


describe("Test de la fonction qui vérifie l'existence de doublons lors de l'utilisation d'une stratégie d'authenfication", () => {

	let userMail, userId, profile, provider;

	//A implémenter selon le scénario en test.
	const mock = jest.spyOn(User, "findOne");

	jest.useFakeTimers(); //Régler le problème de setTimeout

	beforeEach(() => {
		userMail = faker.internet.email();
		userId = faker.random.uuid();
		profile = {};

		jest.clearAllMocks();
	});

	// A supprimer ?
	afterEach(() => {
		mock.mockClear();
	});


	it("should return a user with the state 'VALID' if everything is ok.", (done) => {
		// Arrange
		userMail = "test@test.test";
		provider = "google";

		//Implémentation du User.findOne()
		//Nécessite que findOne soit gérée comme une promesse (
		mock.mockImplementation(() => Promise.resolve({
			email: "test@test.test",
			password: "$2y$12$k56V4Q14TNlOuJde6vvGPu1ZnUj5BjJW4zYTLki.irdHecHmZtc2C",
			google_id: "109437862086042583435"
		}));

		// Act
		const callback = (error, data) => {
			// Assert
			expect(data.state).toEqual(states.VALID);
			done();
		};
		doublonCheck(userMail, "109437862086042583435", profile, provider, callback);
	});

	/**
	 * Techniquement, on est censé ne JAMAIS arriver dans ce cas de figure, sauf en cas d'archi mauvaise manip'.
	 */
	it("should return an error when mail is in base but not provider id", async () => {
		// Arrange
		userMail = "test@test.test";
		provider = "peuImporte";
		mock.mockImplementation(() => Promise.resolve({
			email: "test@test.test",
			password: "$2y$12$k56V4Q14TNlOuJde6vvGPu1ZnUj5BjJW4zYTLki.irdHecHmZtc2C",
			google_id: "109437862086042583435"
		}));

		// Act
		const cb = jest.fn(data => {
			return data;
		});

		await doublonCheck(userMail, userId, profile, provider, cb);// IMPORTANT CAR DOUBLONCHECK EST ASYNC !!

		// Assert
		expect(cb).toHaveBeenCalledWith({error : "Une erreur inconnue est survenue"});
	});

	/**
	 * Techniquement, on est censé ENCORE MOINS arriver dans ce cas de figure, sauf en cas d'attaque de la mort.
	 */
	it("should return a user with the state 'mailConflict' when profile email and then mail of the user in base arent the same.", (done) => {
		// Arrange
		userMail = "tt@test.test";
		provider = "peuImporte";

		mock.mockImplementation(() => Promise.resolve({
			email: "test@test.test",
			password: "$2y$12$k56V4Q14TNlOuJde6vvGPu1ZnUj5BjJW4zYTLki.irdHecHmZtc2C",
		}));

		// Act
		const callback = (error, data) => {
			// Assert
			expect(data.state).toEqual(states.MAILCONFLICT);
			done();
		};

		doublonCheck(userMail, userId, profile, provider, callback);
	});

	it("should return a profile with the state 'canMerge' when profile email is already in base, without providerID", (done) => {
		// Arrange
		userMail = "test@test.test";
		provider = "peuImporte";
		mock
			.mockImplementationOnce(() => Promise.resolve( // Le premier appel à findone ne doit rien renvoyer si on veut tester la seconde partie du code
				false
			))
			.mockImplementationOnce(() => Promise.resolve({
				email: "test@test.test",
				password: "$2y$12$k56V4Q14TNlOuJde6vvGPu1ZnUj5BjJW4zYTLki.irdHecHmZtc2C",
				google_id: "109437862086042583435"
			}));

		// Act
		const callback = (error, data) => {
			// Assert
			expect(data.state).toEqual(states.CANMERGE);
			done();
		};

		doublonCheck(userMail, userId, profile, provider, callback);
	});

	it("should return a profile with the state 'toCreate' when profile email isn't already in base", (done) => {
		// Arrange
		userMail = "test@test.test";
		provider = "peuImporte";
		mock
			.mockImplementationOnce(() => Promise.resolve( // Le premier appel à findone ne doit rien renvoyer si on veut tester la seconde partie du code
				false
			))
			.mockImplementationOnce(() => Promise.resolve(// Le second aussi, pour simuler l'echec de la recherche d'un user avec ce mail
				false
			));

		// Act
		const callback = (error, data) => {
			// Assert
			expect(data.state).toEqual(states.TOCREATE);
			done();
		};

		doublonCheck(userMail, userId, profile, provider, callback);
	});

	/**
	 * C'est un cas très particulier ou par exemple, l'user se connecte avec google, et quelqu'un d'autre le fait avec la même adresse
	 * exactemement en même temps.
	 */
	it("should return an error when we don't find user with this provider id, but when mergin, we find one", async () => {
		// Arrange
		userMail = "test@test.test";
		provider = "peuImporte";
		mock
			.mockImplementationOnce(() =>  Promise.resolve( // Le premier appel à findone ne doit rien renvoyer si on veut tester la seconde partie du code
				false
			))
			.mockImplementationOnce(() => Promise.resolve({
				email: "test@test.test",
				password: "$2y$12$k56V4Q14TNlOuJde6vvGPu1ZnUj5BjJW4zYTLki.irdHecHmZtc2C",
				google_id: "109437862086042583435"
			}));

		const cb = jest.fn(data => {
			return(data);
		});

		// Act
		await doublonCheck(userMail, "109437862086042583435", profile, "google", cb);
		await Promise.resolve(); // arrive quand on a une promise dans une promise ?

		// Assert
		expect(cb).toHaveBeenCalledWith({error : "Une erreur inconnue est survenue"});
	});

});