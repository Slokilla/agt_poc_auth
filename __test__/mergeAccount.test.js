/* eslint-disable no-undef */
const httpMocks = require("node-mocks-http"); // Mocks plus complets pour req et res
const faker = require("faker");
const {merge, finalize} = require("../controller/accountStrat/mergeAccount");
//const finalize = require("../controller/mergeAccount").finalize;
const states = require("../controller/accountStrat/AccountStates");
const User = require("../models/user");

let request, response, next;
next = jest.fn();


describe("merge", () => {

	jest.useFakeTimers();

	beforeEach(() => {
		jest.clearAllMocks();

		request = httpMocks.createRequest({
			method: "GET",
			url: "/auth/google/callback",
			user: {
				id: faker.random.uuid(), //  pas le même format que les ids en base, donc aucune chance qu'il existe !
				state: states.CANMERGE,
			}
		});
		response = httpMocks.createResponse();
	});


	describe("Essayer de fusionner un compte ", () => {

		/*it("Quand la fonction est appelée en dehors d'une requête, elle devrait Throw une exception.", () => {
			expect(() => merge(null, null, null)).toThrow();
		});*/

		it("Quand la methode est appelée avec un user valid, On doit continuer sans encombre",
			() => {
				//Arrange
				request.user = {
					id: faker.random.uuid(),
					state: states.VALID,
				};

				//Act
				merge(request, response, next);

				//Assert
				expect(next).toHaveBeenCalled();
			});

		describe("Quand la methode est appelée avec un user canMerge, On cherche l'user dans la base",
			() => {

				const mock_findOne = jest.spyOn(User, "findOne");

				beforeEach(() => {
					jest.clearAllMocks();
				});

				it("should HTTP 500 an error if the user doesn't exist", async () => {
					// Arrange
					request.user = {
						state: states.CANMERGE,
					};

					mock_findOne.mockImplementationOnce(() => Promise.resolve(
						false
					));

					// Act
					await merge(request, response, next);

					// Assert
					expect(response._getStatusCode()).toEqual(500);
					expect(response._getData()).toEqual({error: "l'utilisateur n'existe pas !"});
				});

				it("should return http 200 when everything is ok", async () => {
					// Arrange
					request.user = {
						temp_id: "5ec3b3431ca4afa16f580d68",//Id de l'utilisateur test@test.test
						state: states.CANMERGE,
						provider: faker.company.companyName(),
						id: faker.random.uuid(),
					};

					mock_findOne.mockImplementationOnce(() => Promise.resolve({
						_id: faker.random.uuid(),
						email: "test@test.test",
						password: "$2y$12$k56V4Q14TNlOuJde6vvGPu1ZnUj5BjJW4zYTLki.irdHecHmZtc2C",
						_doc: {
							google_id: faker.random.uuid(),
							facebook_id: faker.random.uuid(),
							randomValue: faker.random.uuid(),
						}
					}));

					// Act
					await merge(request, response, next);

					// Assert
					expect(response._getStatusCode()).toEqual(200);
				});

				describe("should return http 400 when request misses ...", () => {

					beforeEach(() => {
						mock_findOne.mockImplementationOnce(() => Promise.resolve({
							_id: faker.random.uuid(),
							email: "test@test.test",
							password: "$2y$12$k56V4Q14TNlOuJde6vvGPu1ZnUj5BjJW4zYTLki.irdHecHmZtc2C",
							_doc: {
								google_id: faker.random.uuid(),
								facebook_id: faker.random.uuid(),
							}
						}));
					});

					it("user.id (provider id)", async () => {
						// Arrange
						request.user = {
							temp_id: "5ec3b3431ca4afa16f580d68",//Id de l'utilisateur test@test.test
							state: states.CANMERGE,
							provider: faker.company.companyName(),
						};


						// Act
						await merge(request, response, next);

						// Assert
						expect(response._getStatusCode()).toEqual(400);
						expect(response._getData()).toEqual({error: "Un ou plusieurs argument(s) requis sont null"});
					});

					it("user.provider (provider name)", async () => {
						// Arrange
						request.user = {
							temp_id: "5ec3b3431ca4afa16f580d68",//Id de l'utilisateur test@test.test
							state: states.CANMERGE,
							id: faker.random.uuid(),
						};


						// Act
						await merge(request, response, next);

						// Assert
						expect(response._getStatusCode()).toEqual(400);
						expect(response._getData()).toEqual({error: "Un ou plusieurs argument(s) requis sont null"});
					});
				});
			});
	});//Scénario fusion de compte
});//Tests de la méthode merge

describe("finalize", () => {
	/**
	 * Pour tester cette branche on a besoin de beaucoup de mocks / stub / spy
	 *
	 * D'abord, comme précédement, on a besoin de mock findById, pour simuler la recherche dans la bdd
	 */
	// ATTENTION, on mock findById qui doit du coup renvoyer un User AVEC une méthode save dedans !
	const mock_findById = jest.spyOn(User, "findById");

	beforeEach(() => {
		jest.clearAllMocks();
		request = httpMocks.createRequest({
			method: "GET",
			url: "/auth/merge",
			provider: "google",
			user: {
				id: faker.random.uuid(), //pas le même format que les ids en base; donc aucune chance qu'il existe !
				state: states.CANMERGE,
			},
			login(usr, err) {
				if (!usr) {
					err("Pas d'utilisateur !");
				} else {
					err(null);
				}
			},
		});
		response = httpMocks.createResponse();
	});


	it("should redirect with 303 when everything goes right", async () => {

		// Arrange
		request.query["provider"] = "google";

		mock_findById.mockImplementation(() => Promise.resolve({
			_id: faker.random.uuid(),
			email: "test@test.test",
			password: "$2y$12$k56V4Q14TNlOuJde6vvGPu1ZnUj5BjJW4zYTLki.irdHecHmZtc2C",
			saveable: true,
			save: (cb) => {
				if (!this.saveable) cb();
			},
		}));

		// Act
		await finalize(request, response, next);

		// Assert
		expect(response._getStatusCode()).toEqual(303);
	});

	it("should redirect with 500 and error when save failed", async () => {

		// Arrange
		mock_findById.mockImplementation(() => Promise.resolve({
			_id: faker.random.uuid(),
			email: "test@test.test",
			password: "$2y$12$k56V4Q14TNlOuJde6vvGPu1ZnUj5BjJW4zYTLki.irdHecHmZtc2C",
			saveable: false,
			save: (cb) => {
				if (!this.saveable) cb("Not saveable RN");
			},
		}));

		// Act
		await finalize(request, response, next);

		// Assert
		expect(response._getStatusCode()).toEqual(500);
		expect(response._getData()).toEqual({error: "Une erreur est survenue lors de la sauvegarde"});
	});

	it("should redirect with 500 and error when login failed", async () => {

		// Arrange
		const saveable = true; //On veut que la sauvegarde réussisse
		mock_findById.mockImplementation(() => Promise.resolve({
			_id: faker.random.uuid(),
			email: "test@test.test",
			password: "$2y$12$k56V4Q14TNlOuJde6vvGPu1ZnUj5BjJW4zYTLki.irdHecHmZtc2C",
			save: (cb) => {
				if (saveable) {
					cb(null);
				} else {
					cb("Not saveable RN");
				}
			},
		}));

		// On force login à échouer pour étudier son comportement :
		request.login = (usr, err) => {
			err("Pas d'utilisateur !");
		};

		// Act
		await finalize(request, response, next);

		// Assert
		expect(response._getStatusCode()).toEqual(500);
		expect(response._getData()).toEqual({error: "Une erreur est survenue lors de la connexion"});
	});

	it("should redirect with 500 and error when user doesnt exists", async () => {

		// Arrange
		//On simule le fait qu'aucun user ne correspond en base
		mock_findById.mockImplementation(() => Promise.resolve(false));


		// Act
		await finalize(request, response, next);

		// Assert
		expect(response._getStatusCode()).toEqual(500);
		expect(response._getData()).toEqual({error: "l'utilisateur n'existe pas !"});
	});
});// Tests de la méthode finalize