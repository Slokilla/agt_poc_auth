/*eslint-disable no-unused-vars*/
const User = require("../../models/user");
const states = require("./AccountStates");

/**
 * **A propos de save et login.**
 * Pour bien comprendre les tests unitaires, il convient de bien comprendre le fonctionnement de User.save et de
 * req.login.
 * L'idée est que ces fonction prennent en argument un callback d'erreur **qui est appelé quoi qu'il arrive**.
 * C'est pour cella qu'on test err juste après. Si le code métier a détecté un erreur, il appelera le callback avec un
 * argument décrivant l'erreur. Si tout c'est bien passé, le callback sera appelé avec "null", faisant donc qu'on ne
 * rentre pas dans la condition.
 */

/**
 * Pour le merge des comptes, on a un souci. On ne peut pas renvoyer d'information avec la fonction done() de passport
 * du coup, on a rusé. On a une entrée dans notre table user qui permet de stocker l'état du compte. Ainsi, si on
 * détecte la nécessité de merge un compte, on passe la valeur à "toMerge" et on catch l'état plus tard.
 */
exports.merge = (req, res, next) => {
	if (req.user.state !== states.CANMERGE) {
		next();
	} else {
		User.findOne(req.user.temp_id).then((usr) => {
			if (!usr) {
				res.status(500).send({error: "l'utilisateur n'existe pas !"});
			} else {
				if(!req.user.id || !req.user.provider) {
					return res.status(400).send({error: "Un ou plusieurs argument(s) requis sont null"});
				}
				let infos = {};
				infos.email = usr.email;
				infos.userId = usr._id;
				infos.provider = req.user.provider;
				infos.providerId = req.user.id;
				infos.providersLinked = {};
				//JE ne sais pas si la syntaxe [key,] est correcte
				for (let [key,] of Object.entries(usr._doc)) {
					if (/^[\S]+_id$/.test(key)) {
						infos.providersLinked[key] = null;
					}
				}
				res.status(200);
				res.render("merge", {infos: infos});
			}
		});

	}
};

/**
 * Finalisation d'un merge de compte.
 * En fin de comptes, la fonction ne fait que sauvegarder l'id provider en base.
 * Pour voir la bonne clé, on prend le provider et on lui ajoute "_id". Je n'ai croisé aucune exception
 * à l'heure actuelle.
 * @param req
 * @param res
 * @param next
 * @return {redirect} vers l'accueil ("/")
 */
exports.finalize = (req, res, next) => {
	const key = req.query.provider + "_id";
	User.findById(req.query.userid).then((usr) => {
		if (!usr) {
			res.status(500).send({error: "l'utilisateur n'existe pas !"});
		} else {
			usr[key] = req.query.token;
			usr.state = states.VALID;
			//console.log(usr);
			usr.save(err => {
				if(err) {
					console.log(err);
					return res.status(500).send({error: "Une erreur est survenue lors de la sauvegarde"});
				}
				req.login(usr, err => {
					if (err)
						return res.status(500).send({error: "Une erreur est survenue lors de la connexion"});
					res.redirect(303,"/");// Utiliser http 30X
				});
			});

		}
	});
};