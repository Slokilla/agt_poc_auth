const passport = require("passport");
const GoogleStrategy = require("passport-google-oauth").OAuth2Strategy;
const checkDoublon = require("./doublon_checker").doublonCheck;
require("dotenv").config();

//region Stratégie Google
/**
 * On crée une nouvelle stratégie, appelée google (premier argument)
 * Ensuite, on paramètre une nouvelle googleStrategy.
 *
 * La première étape est de passer en option les identifiants pour taper dans l'api Google.
 * Ensuite, il faut lui dire sur quel adresse renvoyer l'utilisateur authentifié.
 *
 * Ensuite on gère ce qui se passe entre le moment auquel l'utilisateur choisi un compte, et le moment auquel
 * l'utilisateur est redirigé.
 *
 * On check ensuite si il existe des doublons de cette adresse en base
 * @STUB : checkDoublon(
 *              userMail : mail de l'utilisateur,
 *              userId : id de l'utilisateur,
 *              provider : nom du provider en minuscule et sans espace. Il doit correspondre au nom du champ dans la base; avec '_id' en moins,
 *              done : la fonction done, pour terminer la fonction.
 *          )
 *
 * Le scope (informations récupérées) est géré dans la route.
 */
passport.use("google",
	new GoogleStrategy({
		clientID: process.env.GOOGLE_CLIENT_ID,
		clientSecret: process.env.GOOGLE_CLIENT_SECRET,
		callbackURL: "http://localhost:3000/auth/google/callback",
		prompt: "select_account",
		scope: ["email", "profile"],
	},
	(accessToken, refreshToken, profile, done) => {

		const userMail = profile._json.email;
		const userId = profile.id;

		checkDoublon(userMail, userId, profile, "google", done);

	}));
//endregion
