const User = require("../../models/user");
const states = require("./AccountStates");

exports.create = (req, res, next) => {
	if(req.user.state !== states.TOCREATE) {
		next();
	}
	else {
		res.render("index", {title: "creation de compte"});
	}
};