const passport = require("passport");
const LinkedInStrategy = require("passport-linkedin-oauth2").Strategy;

const doublonCheck = require("./doublon_checker").doublonCheck;

require("dotenv").config();

/**
 * Ici, le scope est géré simplement ici, avec le format droit_champ. Il faut tout de même autoriser l'application dans
 * l'onglet "Products" de l'interface developer "https://www.linkedin.com/developers/apps/{Identifiant_de_l'app}/products"
 * Le produit à utiliser est "sign in with linkedin"
 */
passport.use("linkedin",
	new LinkedInStrategy(
		{ 
			clientID: process.env.LINKEDIN_CLIENT_ID,
			clientSecret: process.env.LINKEDIN_CLIENT_SECRET,
			callbackURL: "http://localhost:3000/auth/linkedin/callback",
			scope: ["r_emailaddress", "r_liteprofile"],
		}, (accessToken, refreshToken, profile, done) => {

			const userMail = profile.emails[0].value;
			const userId = profile._json.id;

			doublonCheck(userMail, userId, profile, "linkedin", done);
		}
	));