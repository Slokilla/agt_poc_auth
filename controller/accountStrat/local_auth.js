const bcrypt = require("bcrypt");
const User = require("../../models/user");

const passport = require("passport");
const LocalStrategy = require("passport-local");
 
//region Stratégie Locale
/**
 * On crée une nouvelle stratégie, appelée local (premier argument)
 * Ensuite, on paramètre une nouvelle LocalStrategy.
 *
 * La première étape est de passer en option les nom des champs qui représentent le username et le password
 * de notre utilisateur, ici respectivement email et password.
 *
 * Ensuite on gère ce qui se passe quand l'utilisateur appuie sur le bouton de connexion.
 *
 * On commence par chercher un user lié à cette adresse email.
 * SI on ne trouve pas,
 *     FINI en exécutant done() dans un état d'erreur avec le message "user inconnu"
 * FINSI
 * SI le mot de passe en base est différent du mot de passe rentré par le usr
 *     FINI en ecéxutant done() dans un état d'erreur avec el message "Mot de passe incorrect"
 * FINSI
 * FINAlEMENT,
 *      Si on arrive tout en bas de la fonction, c'est qu'aucun des cas précédents n'est survenu.
 *      @FINI en exécutant done() pour renvoyer l'utilisateur récupéré
 *
 */
passport.use("local",
	new LocalStrategy({
		usernameField: "email",
		passwordField: "password"
	},
	(email, password, done) => {
		User.findOne({email: email}, (err, user) => {
			if (err) {
				return done(err);
			}
			if (!user) {
				//Proposer de créer un compte ?
				console.log("User inconnu");
				return done(null, false, {message: "User inconnu"});
			}
			if(!user.password) {
				console.log("=====Aucun mot de passe enregistré pour l'utilisateur=====");
				return done(null, false, {message: "Aucun mot de passe enregistré"});
			}
			if (!bcrypt.compareSync(password, user.password)) {
				//le mot de passe est incorrect
				console.log("mauvais mdp");
				return done(null, false, {message: "Mdp incorrect"});
			}
			console.log("Connexion en cours ... ");
			done(null, user);
		});
	}
	));
//endregion

