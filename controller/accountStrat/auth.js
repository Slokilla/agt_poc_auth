/* eslint-disable no-unused-vars */
const local = require("./local_auth");
const google = require("./google_auth");
const facebook = require("./facebook_auth");
const linkedin = require("./linkedin_auth");
const twitter = require("./twitter_auth");
const states = require("./AccountStates");
const passport = require("passport");
const jwt = require("jsonwebtoken");
//IMPORTANT POUR DESERIALISER !!!
const User = require("../../models/user");
/**
 * Ce fichier est dédié à la centralisation des stratégie, et de n'avoir qu'un fichier à importer dans app.js.
 * De plus, on défini ici les méthode que passport utilisera pour la sérialisation et la désérialisation.
 * Pour plus d'info :
 * https://stackoverflow.com/questions/27637609/understanding-passport-serialize-deserialize
 */

/**
 * Ici, il est possible que user ne soit pas un user, mais un profil complet (en cas de fusion de compte ou de demande de création).
 * Pour le savoir on teste l'état.
 * Dans le cas ou il vaut "canMerge" il y a un traitement à faire
 */
passport.serializeUser((user, done) => {
	//console.log(user);
	if (user.state === states.CANMERGE) {
		done(null, user);
	} else if (user.state === states.TOCREATE) {
		done(null, user);
	}else {
		console.log("serialisation > ");
		console.log(user);
		done(null, user);
	}
});

/**
 * Le contenu de token varie en fonction du contexte
 * Dans le cas d'un merge de compte, token est le
 * profil de l'utilisateur à intégrer (merci le typage faible :-( )
 * Dans la plupart des cas, token sera un JWT, donc un string.
 * 	Dans ce cas, on vérifie le token, et si il a expiré on reset la session de
 * 	l'utilisateur.
 * 	C'est ici qu'on intégrera un éventuel message "vous avez été déco pour inactivité".
 */
passport.deserializeUser((token, done) => {
	//console.log("désérialisation en cours.");
	//console.log(token);
	if (typeof token === "string") {
		let decodedToken;
		try {
			decodedToken = jwt.verify(token, process.env.JWT_SECRET);
		} catch (e) {
			if (e.name === "TokenExpiredError"){
				console.log("Token expiré le : " + e.expiredAt );
				const user = {error: "expiredToken"};
				return done(null, user);
			}
		}
		const id = decodedToken.userId;
		User.findById(id,
			(err, usr) => {
				if (err) {
					console.warn("Erreur de désérialisation");
					done(err);
				}
				if (!usr) {
					done(null, false, {message: "Aucun utilisateur correspondant"});
				}
				console.log("désérialisation terminée");
				done(null, usr);
			});
	} else {
		done(null, token);
	}
});