const passport = require("passport");
const TwitterStrategy = require("passport-twitter").Strategy;
const checkDoublon = require("./doublon_checker.js").doublonCheck;
require("dotenv").config();

//region Stratégie Twitter
/**
 * Pour le scope, on utilise un userProfileURL, c'est un peu plus lourd...
 * https://github.com/jaredhanson/passport-twitter/issues/67#issuecomment-163769418
 */
passport.use("twitter",
	new TwitterStrategy({
		consumerKey: process.env.TWITTER_CLIENT_ID,
		consumerSecret: process.env.TWITTER_CLIENT_SECRET,
		callbackURL: "http://localhost:3000/auth/twitter/callback",
		userProfileURL: "https://api.twitter.com/1.1/account/verify_credentials.json?include_email=true",
	},
	function (token, tokenSecret, profile, done) {
		//console.log(profile);
		const userMail = profile.emails[0].value;
		const userId = profile.id;

		checkDoublon(userMail, userId, profile, "twitter", done);
	}));
//endregion


