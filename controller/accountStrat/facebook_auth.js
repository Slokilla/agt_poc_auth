const passport = require("passport");
const FacebookStrategy = require("passport-facebook").Strategy;
const checkDoublon = require("./doublon_checker.js").doublonCheck;

require("dotenv").config();

//region Stratégie Facebook
/**
 * Gestion des scopes : https://stackoverflow.com/a/48530267/7821355
 */
passport.use(new FacebookStrategy({
	clientID: process.env.FACEBOOK_CLIENT_ID,
	clientSecret: process.env.FACEBOOK_CLIENT_SECRET,
	callbackURL: "http://localhost:3000/auth/facebook/callback",
	profileFields: ["id", "emails", "name"],
},
function (accessToken, refreshToken, profile, done) {
	const userMail = profile.emails[0].value;
	const userId = profile.id;

	checkDoublon(userMail, userId, profile, "facebook", done);
}));
//endregion