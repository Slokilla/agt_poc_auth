module.exports = Object.freeze({
	VALID: "valid",
	CANMERGE: "canMerge",
	MAILCONFLICT: "mailConflict",
	TOCREATE: "toCreate"
});

