const User = require("../../models/user");
const states = require("./AccountStates");
const jwt = require("jsonwebtoken");

/**
 * On cherche un utilisateur qui est déjà lié à ce token {provider}
 * SI on trouve un utilisateur avec ce token ,
 *      SI l'adresse mail du compte est **DIFFERENTE** du mail lié à {provider},
 *          --> Ici on a un souci. Le token qu'on a récupérer a déjà été lié à une adresse mail.
 *              Il faut à ce moment proposer à l'utilisateur de se connecter avec le compte correspondant
 *              à l'adresse mail en base, et ensuite, lui proposer lequel des deux compte il veut garder (plus simple
 *              que de gérer plusieurs adresses mail, même si ça reste une possibilité)
 *              FINI en exécutant la fonction done() avec un statut d'échec (deuxième paramètre à false) et le type
 *              tokenExisting.
 *      SINON,
 *          --> Tout s'est bien passé !
 *              @FINI en exécutant la fonction done() qui renvoie l'user récupéré au callback
 *      FINSI
 * SINON
 *      On cherche un utilisateur lié à l'adresse mail du compte {provider}
 *      SI on trouve un compte,
 *          --> Il est forcément sans {provider}_id, donc on propose à l'utilisateur de lier le-dit compte au compte
 *              {provider} qu'il vient de choisir.
 *              FINI en exécutant la fonction done() qui renvoie l'utilisateur et le type "linkingAccount"
 *      SINON,
 *          --> Là il faut proposer à l'utilisateur de créer un compte (en fait, de choisir un mot de passe car le
 *              formulaire sera prérempli grâce aux infos de {provider}.
 *      FINSI
 * FINSI
 *
 * L'avantage principal de se casser la tête avec cette méthode, c'est qu'en bout de route on a UN compte par user,
 * mais que l'user peut se connecter de X façons différentes.
 * @param userMail le mail de l'utilisateur
 * @param userId l'id de l'utilisateur chez ce provider
 * @param provider le nom du provider en minuscule et sans espace, il doit correspondre au nom du champs dans la table utilisateur en base, mais sans '_id'
 * @param done la fonction qui termine l'appel
 */

//Voir à rajouter un X.catch();
exports.doublonCheck = (userMail, userId, profile, provider, done) => {
	const provider_id = provider + "_id";
	User.findOne({[provider_id]: userId}).then((user) => {
		if (user) {
			if (user.email !== userMail) {
				//Il y a un soucis, on a là deux mail pour le même {provider}_id. Il faut proposer
				//de lier les comptes, en gardant en tête qu'un seul peut être conservé
				user.state = states.MAILCONFLICT;
				done(null, user);
			} else if (user[provider_id] === userId) {
				//Parfait ! On renvoie le token d'authentification.
				user.state = states.VALID;
				done(null, user);
			}
			else {
				done({error : "Une erreur inconnue est survenue"});
			}
		} else {
			User.findOne({email: userMail}).then((user) => {
				/*if (err) {
					done(err);
				}*/
				// TODO On invite l'utilisateur à crée un compte à partir des informations {provider} ?
				if (!user) {
					profile["state"] = states.TOCREATE;
					done(null, profile);
				} else if(!user[provider_id]){
					// On propose à l'utilisateur lier un compte ?
					profile["temp_id"] = user._id;
					profile["state"] = states.CANMERGE;
					done(null, profile);
				} else {
					done({error : "Une erreur inconnue est survenue"});
				}
			});//Fin User.findOne() sur l'email
		}//Fin else
	});//Fin de User.findOne() sur le provider_id
};//Fin déclaration de doublonCheck()