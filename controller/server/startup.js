const Authentication = require("../../models/authentication");

exports.clearConnections =  async () => {
	await Authentication.find({isExpired: false})
		.then(docs => {
			docs.forEach(doc => {
				doc.isExpired = true;
				doc.save();
			});
		})
		.catch(err => {
			console.log(err);
		});
	console.log("fin du clear de connexion");
};