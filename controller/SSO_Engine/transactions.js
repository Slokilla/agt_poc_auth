const Authentication = require("../../models/authentication");
const User = require("../../models/user");


async function checkExpirency(auth) {
	if (Date.now() > auth.expiresAt) {
		auth.isExpired = true;
		await auth.save()
			.then(err => {
				if (err) {
					console.log(err);
					return {err: "erreur durant la sauvegarde"};
				}
			});
	}
}

// Correspondance diagramme SSO : S1
exports.isAuth = async (req, res, next) => {
	if (req.query.t !== undefined) {

		console.log(req.query.t);

		if (req.query.t === "undefined") {
			console.log("La requête est nulle");
			return res.status(200).json({isAuth: false, reason: "noRequest"});
		}

		await Authentication.findOne({SSOToken: req.query.t})
			.then((auth) => {
				console.log("j'ai trouvé");
				if (!auth) {
					console.log("Pas d'authentification active correspondant au token !");
					req.logout();
					return res.status(200).json({isAuth: false, reason: "noActiveToken"});
				} else {
					checkExpirency(auth).then(err => {
						if (err) {
							console.log("Une erreur a été détectée durant la verification de l'expiration du token");
							req.logout();
							return res.status(500).send(err);
						}
					});

					if (auth.isExpired) {
						console.log("expired");
						req.logout();
						return res.status(200).json({isAuth: false, reason: "expired"});
					}
				}
				return res.status(200).json({isAuth: true});
			})
			.catch(err => {
				console.log(err);
				return res.send("Une erreur est survenue");
			});
	} else {
		return res.json({error: "Aucun token"});
	}
};

// Correspondance diagramme SSO : S2
exports.profile = (req, res, next) => {
	const requestToken = req.query.t;
	if (requestToken != null) {
		console.log("Token = " + requestToken);
		Authentication.findOne({SSOToken: requestToken})
			.then(auth => {
				if (auth == null) {
					console.log("no Auth has been found");
					return res.status(404).json({error: "authentification token not registered"});
				}
				User.findById(auth.userId)
					.then(usr => {
						return res.json({email: usr.email, state: usr.state});
					})
					.catch(err => {
						console.warn("Une erreur est survenue lors dans la recherche d'un utilisateur");
						console.warn(err);
					});
			})
			.catch(err => {
				console.warn("Une erreur est survenue lors dans la recherche dans l'historique d'auth");
				console.warn(err);
			});

	}
};