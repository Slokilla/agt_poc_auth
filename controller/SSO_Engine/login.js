const {allowedOrigins} = require("./allowedOrigins.js");
const md5 = require("md5");
const Authentication = require("../../models/authentication");
const {v4: uuidv4} = require("uuid");

/**
 * Méthode principale du SSO, c'est la porte d'entrée de toute les demandes de connexions.
 * Voilà le workflow :
 * 	SI le programme ne connais pas ServiceURL (l'url du service duquel est issu la demande de connexion)
 * 		SI on a un origin en session (donc si la demande est déjà en cours de traitement)
 * 			On la récupère.
 * 		SINON SI on a un paramètre GET qui s'appelle serviceUrl
 * 			On récupère	sa valeur.
 * 		FINSI
 * 	SI on a réussi à récupérer un serviceUrl,
 * 		On doit le formater pour en faire une URL
 * 		SI cet URL n'est pas autorisé expressément dans la liste des origines admises,
 * 			on rejette la demande de connexion.
 * 		FINSI
 * 	FINSI
 * 	SI le client n'a pas d'origine,
 * 		On rejette sa demande
 * 	FINSI
 *
 * 	Ensuite on a le coeur névralgique du sso
 *
 * SI l'utilisateur s'est déjà connecté, on regarde la validité de son token, qui est stocké en session
 * 		N.B. Pour cela, il faut que le serviceUrl soit valide et ne soit pas modifier pendant l'opération
 *		C'est pour cela qu'avant tout, on verifie si l'empreinte correspond
 * FIN SI
 */
exports.login = async (req, res, next) => {

	console.warn("Session user :");
	console.warn(req.session.user);
	console.warn("Req user :");
	console.warn(req.user);
	console.warn("Req session origin :");
	console.log(req.session.origin);


	let serviceUrl;

	if (typeof serviceUrl === "undefined") {
		//Dans le cas ou l'on récupère le serviceUrl depuis la session (donc déjà par ici)
		if (req.session.origin) {
			serviceUrl = req.session.origin;
			req.session.print = md5(serviceUrl); // pour vérifier plus tard l'intégrité du serviceUrl
		}
		//dans le ca ou l'on récupère l'origine depuis l'url (donc on viens juste de cliquer sur un lien.
		else if (req.query.serviceUrl) {
			serviceUrl = req.query.serviceUrl;
		} else {
			//A SUPPRIMER
			serviceUrl = null;
		}
	}

	// On formate l'url de l'origine du client. On en profite pour vérifier si l'origine est autorisée ou non.
	// On travaille ici avec le fonctionnement "Whitelist". On ne laisse passer que les requêtes dont les origines
	// ont été expressément validées.
	if (serviceUrl) {
		const allowedOrigin = allowedOrigins();
		const url = new URL(serviceUrl);

		if (allowedOrigin[url.origin] !== true) {
			return res.json({message: "Vous n'êtes pas autorisé"});
		}
	}

	//Interface de gestion du compte en général.
	if (req.user != null && (serviceUrl === undefined || serviceUrl == null)) {
		console.log("interface de gestion de compte => ");
		console.log(req.user);
		return res.send({message: "Not implemented RN"});
	}

	/**
	 * ATTENTION :
	 * undefined != null  // false
	 * undefined !== null // true
	 */
	if (req.user != null && serviceUrl != null) {
		console.log("login > req.user :");
		console.log(req.user);
		console.log("login > req.session.user :");
		console.log(req.session.user);

		// Si le serviceUrl a changé en plein milieu du traitement.
		/*if (md5(serviceUrl) !== md5(req.session.origin))
			return res.send("L'url d'origine a changé durant l'opération. Veuillez recommencer");*/

		const redirectUrl = new URL(serviceUrl);
		const ssoToken = uuidv4(); // Génération d'un UUID selon la RFC 4122 https://tools.ietf.org/html/rfc4122

		//Si c'est notre premier passage ici, notre token de sso est undefined
		// Correspondance diagramme SSO : S4
		if (req.user.ssoToken === undefined) {
			req.session.user = {
				id: req.user._id,
				ssoToken: ssoToken,
				origin: req.session.origin,
				registeredAt: Date.now(),
			};

			// Une fois l'origine sauvegardée, on la remet à zéro pour les prochaines tentatives de connexion
			req.session.origin = null;

			//on garde une trace en base
			const auth = new Authentication({
				userId: req.user._id,
				SSOToken: ssoToken,
				origin: redirectUrl,
				connectedAt: Date.now(),
				expiresAt: Date.now() + 1000 * 60 * 60,
				isExpired: false
			});

			await auth.save().then(() => {
				req.login(req.session.user, err => {
					if (err) {
						console.warn("erreur login: ");
						console.warn(err);
						return res.send({error: "Erreur durant req.login"});
					}
				});
				console.log("after req login > req user ");
			}).catch(err => {
				console.warn("erreur save:");
				console.warn(err);
				if (err) return res.send(err);
			});


		}
		// Correspondance diagramme SSO : S5
		const redir = (`${redirectUrl}?ssoToken=${req.user.ssoToken}`);
		return res.redirect(redir);
	}

	//On sauvegarde l'origine du client dans sa session et on l'invite à se connecter.
	// Correspondance diagramme SSO : S3
	req.session.origin = serviceUrl;
	console.log(req.session.origin);
	return res.render("auth", {title: "Connexion"});
};