exports.allowedOrigins = () => {
	return {
		"http://localhost:3001": true,
		"http://localhost:3002": true,
		"http://localhost:3000": true,
		"http://127.0.0.1:3002": true,
		"http://127.0.0.1:3000": true,
		"http://127.0.0.1:3001": true,
		"http://ssoclient.local:3002": true,
		"http://ssoclientbis.local:3002" : true,
	};
};
