/**
 * Important pour Mongoose, sans ça l'environnement par défaut est jsdom, et
 * il surcharge des méthodes de base de JS que mongoose utilise.
 * Plus d'infos :
 * https://mongoosejs.com/docs/jest.html
 * @type {{testEnvironment: string}}
 */
module.exports = {
	testEnvironment: "node",
	collectCoverage: true,
	coverageDirectory: "./__test__/CoCo", // Le dossier dans lequel on trouvera une page HTML avec un graph de CoCo
};